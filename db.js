const { Client } = require("pg");
const admin = require("firebase-admin");

var serviceAccount = require("./admin.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://virumaa-checkpoints.firebaseio.com",
});

var db = admin.database();
var ref = db.ref("checkpoints");

const client = new Client({
  user: "t203860",
  host: "dev.vk.edu.ee",
  database: "dbIvleva",
  password: "t203860",
  port: 5432,
});
client.connect();

function getNotBegunGames() {
  return new Promise(function (resolve, reject) {
    client.query(
      "SELECT * FROM projectiot.game WHERE projectiot.game.status = 'not_begun' ORDER BY id ASC ",
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve(results.rows);
      }
    );
  });
}

function getActiveGames() {
    return new Promise(function (resolve, reject) {
        client.query(
            "SELECT * FROM projectiot.game WHERE projectiot.game.status = 'active' ORDER BY id ASC ",
            (error, results) => {
                if (error) {
                    resolve();
                }
                resolve(results.rows);
            }
        );
    });
}

function newGame(body) {
  return new Promise(function (resolve, reject) {
    client.query(
      "INSERT INTO projectiot.game (title, description, country, place, date_game, time_begin, map, distance, status) VALUES ('" +
        body.title +
        "','" +
        body.description +
        "','" +
        body.country +
        "','" +
        body.place +
        "','" +
        body.date_game +
        "','" +
        body.time_begin +
        "','" +
        body.map +
        "','" +
        body.distance +
        "','" +
        "not_begun" +
        "')",
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve();
      }
    );
  });
}

function newCheckpoint(body) {
  return new Promise(function (resolve, reject) {
    var columns = 'game_id, checkpoint_id, "order", longitude, latitude';
    client.query(
      "INSERT INTO projectiot.game_checkpoints (" +
        columns +
        ") VALUES ('" +
        body.game_id +
        "','" +
        body.checkpoint_id +
        "','" +
        body.order +
        "','" +
        body.longitude +
        "','" +
        body.latitude +
        "')",
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve();
      }
    );
  });
}

function setActive(body){
    return new Promise(function (resolve, reject) {
        client.query(
            "UPDATE projectiot.game SET status='active' WHERE id = "+body.id+"",
            (error, results) => {
                if (error) {
                    reject(error);
                }
                resolve();
            }
        );
    });
}

function finishGame(body) {
    var checkpoints = [];
    client.query(
        "SELECT * FROM projectiot.game_checkpoints WHERE game_id="+body.game_id,
        (error, results) => {
            if (error) {
                console.log(error);
            }
            checkpoints = results.rows;
        }
    );

    ref.once(
      "value",
      function (snapshot) {
          checkpoints.forEach( element => {
                  snapshot.forEach(function (childSnapshot) {
                      var data = childSnapshot.val();
                      if (element.id == data.game_checkpoint_id) {
                          client.query(
                              "INSERT INTO projectiot.player_checkpoints (player_id, game_checkpoint_id, time) VALUES ('" +
                              data.player_id +
                              "','" +
                              data.game_checkpoint_id +
                              "','" +
                              data.time +
                              "')",
                          );
                      }
                      //ref.remove();
                  });
          }
      )
      },
      function (errorObject) {
        console.log("The read failed: " + errorObject.code);
      }
    );

      client.query(
          "UPDATE projectiot.game SET status='finished' WHERE id = "+body.game_id+"",
      );
}

module.exports = {
  getNotBegunGames,
  getActiveGames,
  setActive,
  newGame,
  newCheckpoint,
  finishGame,
};
