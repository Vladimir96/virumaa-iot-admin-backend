const express = require("express");
const cors = require("cors");
const app = express();

const port = process.env.PORT || 3001;

const db = require("./db");
app.use(cors());

app.use(express.json());
app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Methods", "GET,POST");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Content-Type, Access-Control-Allow-Headers"
  );
  next();
});

app.get("/getNotBegunGames", (req, res) => {
  db.getNotBegunGames()
    .then((response) => {
      res.status(200).send(response);
    })
    .catch((error) => {
      res.status(500).send(error);
    });
});

app.get("/getActiveGames", (req, res) => {
  db.getActiveGames()
      .then((response) => {
        res.status(200).send(response);
      })
      .catch((error) => {
        res.status(500).send(error);
      });
});

app.post("/games", (req, res) => {
  db.newGame(req.body)
    .then((response) => {
      res.status(200).send(response);
    })
    .catch((error) => {
      res.status(500).send(error);
    });
});

app.post("/checkpoints", (req, res) => {
  db.newCheckpoint(req.body)
    .then((response) => {
      res.status(200).send(response);
    })
    .catch((error) => {
      res.status(500).send(error);
    });
});

app.post("/finish", (req, res) => {
  db.finishGame(req.body);
    res.status(200).send();
});

app.post("/setActive", (req, res) => {
  db.setActive(req.body)
      .then((response) => {
        res.status(200).send(response);
      })
      .catch((error) => {
        res.status(500).send(error);
      });
});

app.listen(port, () => {
  console.log(`App running on port ${port}.`);
});
